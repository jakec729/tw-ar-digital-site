# Time Warner Year in Review 2017 (AR Digital)
This site was designed to display highlights from Time Warner properties throughout 2017.

## About the site
The site was developed using [Jekyll](jekyllrb.com) to organize HTML into DRY templates using `includes` and `layouts`. Dependencies are managed through [Yarn](https://github.com/yarnpkg/yarn). Assets are packaged with Webpack via [Laravel Mix](https://github.com/JeffreyWay/laravel-mix).


## Working with the code

Live preview of latest commit hosted here:
https://objective-sinoussi-14b525.netlify.com/

To change the root URL, update `_config.yml`:
```
url: # http://some-new-url.com
```

### Install Dependencies
```
yarn install
```

### Generate static site
```
bundle exec jekyll build
```

### Generate and preview site through localhost
```
bundle exec jekyll serve
```

### Generate Assets & Watch for changes
```
npm run watch
```